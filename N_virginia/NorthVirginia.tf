terraform {
  backend "local" {
    path = "./state/terraform.tfstate"
  }
}

provider "aws" {
  access_key = "${var.access}"
  secret_key = "${var.secret}"
  region = "${var.region}"
}

module "frankfurt" {

  source = "../module"
  vpc_cidr = "${var.vpc_cidr}"
  private_subnet_cidr = "${var.private_subnet_cidr}"
  public_subnet_cidr = "${var.public_subnet_cidr}"
}

output "dns" {
  value = "${module.frankfurt.dns}"
}