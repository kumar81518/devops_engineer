## Solution of the requirements

Direction on how to use this terraform code base

Step1: Clone this repo using below command
```bash
git clone https://gitlab.com/kumar81518/devops_engineer.git
cd devops_engineer
```

Step2: Download docker on your ubuntu box.
```bash
sudo snap install docker
sudo usermod -aG docker $USER
```
after exection please restart the box and continue below steps

Step3: Build the docker image using Dockerfile in the root of the repo
```bash
docker build -t terraform .
```

Verify step3 using "docker images | grep terraform" command

Step4: launch container using terraform image and goto ~/code/devops_engineer directory 
```bash
docker run -it terraform /bin/bash
root@your_container_id:/# cd ~/code/devops_engineer
```

Step5: goto frankfurt or N_virgia folder then create terraform.tfvars file and put your secrets and variable values.  

##Below is the description about the variable in terraform.tfvars  

| variable_name     | description                                   |
|:-----------------:|:---------------------------------------------:|
|access             |Give your access key value here                |
|secret             |Give your secret_access key value here         |
|region             |Give region you want to target                 |
|vpc_cidr           |Give CIDR range of the vpc you want to create  |
|private_subnet_cidr|Give private subnet CIDR range here            |
|public_subnet_cidr |Give public subnet range here                  |

Step6: run the mylaunch.sh script to provision the infra  
please consider that this script accepts one argument which is the name of the folder where you are using the module.  
In this case, value of argument can be either frankfurt or N_virginia
```bash
./mylaunch.sh frankfurt
```
Step7: upon completion please execute below command to see the output vars.

```bash
terraform output dns
```
this command will give dns of the elb that we are creating infront of the auto scaling group instances.
if you go on this dns you should see the default nginx welcome page