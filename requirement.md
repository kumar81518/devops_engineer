# People Fluent Devops_engineer

A technical test for the recruitment of new DevOps engineers.

## Requirements

* Terraform v0.11.7
* Access to an AWS account - *to run a Terraform plan for verification. You are not expected to provision the instances or incur any costs.*

## Overview

In this repository you will find some Terraform config for the deployment of
AWS infrastructure into a data centre based in Ireland.

Please go through the required section below, completing each task. Part of the assessment involves your use of source code management tools, so please do not just do one big commit upon completion, it is important that we see your workflow.

Please do not take any longer than 5-6 hours on this - if you are unable to complete all the required tasks please add a file or comments where appropriate to explain how you would complete the required tasks.

You do not need to worry about remote state for this - local is fine.

## Tasks

### Required

1. Our clients based in the US have complained about the speed of page loads, so we'd like to set up identical infrastructure based closer to them. Please modify the config here to make that possible and to allow us to repeat that process in the future for other locations. *Please note that in this scenario there is no need for storage across the regions.*
2. Add a Dockerfile to ensure that the Terraform version running this configuration stays consistent.
3. We'd like to ensure that our SSH access to the machine is as secure as possible. Please add config for a bastion server and amend the config for our instance so it is not accessible over the internet.

### *Bonus round*

1. Add a script in a language of your choice to automate running Terraform for a new DC, injecting variables as appropriate.
2. The application is not very scalable nor is it redundant in it's current form. Please add configuration to launch it into an auto-scaling group. The output should still return one endpoint which should show the nginx default landing page.
3. Add Cloudformation config to set up an s3 bucket for future remote state configuration.
