resource "aws_vpc" "main" {
  cidr_block           = "${var.vpc_cidr}"
  enable_dns_support   = true
  enable_dns_hostnames = true
}

# for the main instance which needs to be accessed by bastion host
resource "aws_subnet" "main_private" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "${var.private_subnet_cidr}"

  tags {
    Name = "Main_private"
  }
}

# for the bastion host which will be exposed to internet
resource "aws_subnet" "main_public" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "${var.public_subnet_cidr}"

  tags {
    Name = "Main_public"
  }
}


resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name = "igw_main"
  }
}

# creating eip for the nat gatway
resource "aws_eip" "ig" {
  vpc      = true
}

#creating netgateway for the internet to private subnet
resource "aws_nat_gateway" "this" {

  allocation_id = "${aws_eip.ig.id}"
  subnet_id     = "${aws_subnet.main_public.id}"

  tags { Name = "nat-gateway"}

  depends_on = ["aws_internet_gateway.gw"]
}


# public subnet route table
resource "aws_route_table" "main" {
  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name = "main_public"
  }
}

# internet route to public subnet
resource "aws_route" "route_internet" {
  route_table_id         = "${aws_route_table.main.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.gw.id}"
}

resource "aws_route_table_association" "public_dmz" {
  subnet_id      = "${aws_subnet.main_public.id}"
  route_table_id = "${aws_route_table.main.id}"
}

resource "aws_route_table" "main_private" {
  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name = "main_private"
  }
}

# internet route for the private subnet
resource "aws_route" "nat_route_internet" {
  route_table_id         = "${aws_route_table.main_private.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_nat_gateway.this.id}"
}