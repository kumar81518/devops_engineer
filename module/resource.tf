# code assumes that we have instance_key and bastion_key available in aws account
resource "aws_instance" "frankfurt" {
  ami                         = "ami-c7e0c82c"
  instance_type               = "t2.micro"
  user_data                   = "${file("../cloud-config.yml")}"
  security_groups             = ["${aws_security_group.main_inst.id}"]
  associate_public_ip_address = true
  subnet_id                   = "${aws_subnet.main_private.id}"
  key_name = "instance_key"
  root_block_device {
    volume_size = 10
  }
}

output "dns_endpoint" {
  value = "${aws_instance.frankfurt.public_dns}"
}

resource "aws_instance" "bastion" {
  ami                         = "ami-c7e0c82c"
  instance_type               = "t2.micro"
  #user_data                   = "${file("cloud-config.yml")}"
  security_groups             = ["${aws_security_group.bastion.id}"]
  associate_public_ip_address = true
  subnet_id                   = "${aws_subnet.main_public.id}"
  key_name = "bastion_key"

  root_block_device {
    volume_size = 10
  }
}