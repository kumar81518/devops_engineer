resource "aws_elb" "auto_elb" {
  name            = "myelb"
  subnets         = ["${aws_subnet.main_public.id}"]
  security_groups = ["${aws_security_group.bastion.id}"]
  internal        = false

  listener {
    instance_port     = 80
    instance_protocol = "tcp"
    lb_port           = 80
    lb_protocol       = "tcp"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:80"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags {
    Name    = "Auto_Scaling_Group_elb"
  }
}
resource "aws_launch_template" "auto_launch_config" {
  name = "my_launch_config"
  image_id                       = "ami-c7e0c82c"
  instance_type               = "t2.micro"
  user_data                   = "${file("../cloud-config.yml")}"
  vpc_security_group_ids             = ["${aws_security_group.main_inst.id}"]
  key_name = "instance_key"
}

resource "aws_autoscaling_group" "my_auto_scaling_group" {
  name  = "terraform-autoscale"
  desired_capacity = 1
  max_size = 5
  min_size = 2
  vpc_zone_identifier = ["${aws_subnet.main_private.id}"]
  launch_template = {
    id = "${aws_launch_template.auto_launch_config.id}"
    version = "$$Latest"
  }
  load_balancers = ["${aws_elb.auto_elb.name}"]
}

output "dns" {
  value = "${aws_elb.auto_elb.dns_name}"
}

output "arn" {
  value = "${aws_elb.auto_elb.arn}"
}
