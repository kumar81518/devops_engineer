FROM ubuntu
RUN apt-get update -y && \
    apt-get install -y git vim curl unzip && \
    cd /tmp && \
    curl https://releases.hashicorp.com/terraform/0.11.7/terraform_0.11.7_linux_amd64.zip -o terraform.zip && \
    unzip terraform.zip && \
    rm -rf terraform.zip && \
    echo "export PATH=$PATH:/tmp" >> ~/.bashrc && \
    mkdir ~/code && \
    cd ~/code && \
    git clone https://gitlab.com/kumar81518/devops_engineer.git