#!/usr/bin/env bash

cd ./$1/
if [ ! -d ".terraform" ];then
    terraform init
fi

if [ ! -f "terraform.tfvars" ];then
    echo "Please create terraform.tfvars file."
    exit 1
fi

terraform plan

echo "Does Plan look good to you? [y/n]:"
read n

if [ n == "y" ] || [ n == "Y" ];then
    terraform apply
else
    echo "please change the config according to your need"
fi
